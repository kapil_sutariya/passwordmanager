/**
 * Created by Kapil on 8/16/2015.
 */
angular.module('PasswordManager.utility',[])
.factory('PageService', function() {
    var savedData = {}
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }

    return {
        set: set,
        get: get
    }
});