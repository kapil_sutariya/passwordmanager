
angular.module('passwordManagerApp', ['ngAnimate','PasswordManager.router','PasswordManager.controller','PasswordManager.focus'])
    .animation('.lazyEffect', function(){
        return {
            enter : function(element, done) {
                element.css('display', 'none');
                setTimeout(function(){
                    element.fadeIn(350,done);
                },150);

                return function(){};
            },
            leave : function(element, done){
                element.fadeOut(150,done);
                return function(){};
            }
        }
    });