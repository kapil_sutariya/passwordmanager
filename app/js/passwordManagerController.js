
angular.module('PasswordManager.controller',['PasswordManager.repository','PasswordManager.utility'])
.controller('passwordManagerController',['$scope','$location', function($scope, $location){
    $scope.addPassword = function(){
        $location.url('addPage');
    }
    $scope.searchPassword = function(){
        $location.url('searchPage');
    }
}])
.controller('addPageController',['$scope','$location','PageRepository', function($scope, $location, PageRepository){
    $scope.addPage = function(){
        if($scope.category !== undefined && $scope.page !== undefined && $scope.user !== undefined && $scope.password !== undefined){
            var page = {category : $scope.category, page: $scope.page, user: $scope.user, password : $scope.password};
            PageRepository.save(page);
            $location.url('searchPage');
        }
    }
}])
.controller('searchPageController',['$scope','$location','PageRepository','CategoryRepository','PageService', function($scope, $location, PageRepository, CategoryRepository, PageService){
        $scope.model = { id: 'All' }

        $scope.pageListTemp = [{}];
        $scope.pageList = [];
        CategoryRepository.query(function(result){
            $scope.categoryList = result;
        });

        PageRepository.query(function(result){
            $scope.pageList = result;
        });

        $scope.categoryFilter = function (value) {
            if($scope.model.id === 'All' || angular.lowercase(value.category) === angular.lowercase($scope.model.id)){
                return true;
            }
            else {
                return false;
            }
        }

        $scope.setFocus = function(index){
            //document.getElementById("input-"+index).focus();
        }

        $scope.showPage = function(page){
            $scope.page = page;
        }

        $scope.updatePage = function(page){
            PageRepository.update(page);
        }

        $scope.deletePage = function(page){
            var id = {id : page.id}
            PageRepository.delete({},page, function(data){
                $scope.pageList = $scope.pageList.filter(function (obj) {
                    return obj.id !== page.id;
                });
            });
        }
        
        $scope.openNewTab = function () {
            if($scope.page !== undefined){
                PageService.set($scope.page);
                $location.url('viewPage');
            }
        }
}])
.controller('viewPageController',['$scope','$location','PageRepository','PageService', function($scope, $location, PageRepository,PageService){

        $scope.page = PageService.get();
        $scope.displayPassword = '';

        if(!isEmpty($scope.page)){
            for( i = 0; i < $scope.page.password.length; i++)
            $scope.displayPassword += "*";
        }

        $scope.copyPassword = function() {
            copyTextToClipboard($scope.page.password);
        }

        function isEmpty(obj) {
            for(var prop in obj) {
                if(obj.hasOwnProperty(prop))
                    return false;
            }

            return true;
        }

        function copyTextToClipboard(text) {
            var textArea = document.createElement("textarea");

            // Place in top-left corner of screen regardless of scroll position.
            textArea.style.position = 'fixed';
            textArea.style.top = 0;
            textArea.style.left = 0;

            // Ensure it has a small width and height. Setting to 1px / 1em
            // doesn't work as this gives a negative w/h on some browsers.
            textArea.style.width = '2em';
            textArea.style.height = '2em';

            // We don't need padding, reducing the size if it does flash render.
            textArea.style.padding = 0;

            // Clean up any borders.
            textArea.style.border = 'none';
            textArea.style.outline = 'none';
            textArea.style.boxShadow = 'none';

            // Avoid flash of white box if rendered for any reason.
            textArea.style.background = 'transparent';


            textArea.value = text;

            document.body.appendChild(textArea);

            textArea.select();

            try {
                var successful = document.execCommand('copy');
            } catch (err) {
                console.log('Oops, unable to copy');
            }

            document.body.removeChild(textArea);
        }
}])
;