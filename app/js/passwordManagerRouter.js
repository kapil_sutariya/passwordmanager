
angular.module('PasswordManager.router',['ngRoute'])
.config(function($routeProvider){

        $routeProvider
            .when('/searchPage', {
                templateUrl : 'view/searchPage.html',
                controller: 'searchPageController'
            })
            .when('/addPage', {
                templateUrl : 'view/addPage.html',
                controller: 'addPageController'
            })
            .when('/viewPage', {
                templateUrl : 'view/viewPage.html',
                controller: 'viewPageController'
            })
            .otherwise({
                redirectTo: '/searchPage'
            });
    });