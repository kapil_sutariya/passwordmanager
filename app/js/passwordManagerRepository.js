/**
 * Created by Kapil on 8/14/2015.
 */
angular.module('PasswordManager.repository', ['ngResource'])
.factory('PageRepository',["$resource", function($resource){
    return $resource('../api/pages/:pageId', {pageId:'@id'}, {'query': {method:'GET', isArray:true}, 'update' : { method : 'PUT'}, 'delete': { method:'DELETE' },'save':   {method:'POST'}, });
}])
.factory('CategoryRepository',["$resource", function($resource){
    return $resource('../api/category', {'query': {method:'GET', isArray:true} });
}]);