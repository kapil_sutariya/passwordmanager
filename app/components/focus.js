/**
 * Created by Kapil on 8/16/2015.
 */
angular.module('PasswordManager.focus',[])
.directive('customAutofocus', function() {
        return{
            restrict: 'A',

            link: function(scope, element, attrs){
                scope.$watch(function(){
                    return scope.$eval(attrs.customAutofocus);
                },function (newValue){
                    if (newValue == true){
                        element[0].focus();
                    }
                });
            }
        };
    });