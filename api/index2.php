<?php
/*
 * @author Kapil
 */

require_once "vendor/autoload.php";
require_once 'Entities/User.php';
require_once 'Entities/Books.php';
require_once 'Entities/Notifyme.php';
require_once 'Services/AbstractService.php';
require_once 'Services/BookService.php';
require_once 'Services/UserService.php';
require_once 'Services/NotifyMeService.php';
require_once 'Services/MailService.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

$app->get('/books','bookService:get');
$app->post('/login', function() use ($app){
	$user = json_decode($app->request->getBody());
	if(isset($user->email) && isset($user->password)){
		$user = UserService::getService()->verifyLogin($user->email,$user->password);
		if(count($user) > 0 ){
			$app->response->headers['Content-Type'] = 'application/json';
			echo json_encode($user);
		}
		else{
			echo "Login Invalid";
		}
	}
	else{
		echo "Error Page";
	}
});

$app->post('/notifyme',function() use ($app){
	$notifyme = json_decode($app->request->getBody());
	if(isset($notifyme->email) && isset($notifyme->number) && isset($notifyme->city)){
		$user = NotifyMeService::getService()->insert($notifyme->email,$notifyme->number,$notifyme->city);
	}
	else{
		echo "Error Page";
	}
});

$app->get('/mail',function(){
	echo "From mail";
	MailService::getService()->sendMail('ayruamtikna01@gmail.com','Ankit');
});
//$app->get('/users','userService:getAll');

/*$app->get('/check', 'checkMySQL');

$app->get('/checkPDO', function(){
	
		include_once 'config.php';
		
	    // connect to the database
        $dbh = new PDO("mysql:host=sharemyreads.com;dbname=sharemyreads", DB_USERNAME, DB_PASSWORD);

        // a query get all the records from the users table
        $sql = 'SELECT id, name, email FROM users';

        // use prepared statements, even if not strictly required is good practice
        $stmt = $dbh->prepare( $sql );

        // execute the query
        $stmt->execute();

        // fetch the results into an array
        $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

        // convert to json
        $json = json_encode( $result );
		echo $json;
});

$app->get('/insertUser',function(){
	require_once "bootstrap.php";	
	$users = new Users();
	$users->setName('Kapil');
	$users->setEmail('kapil.sutariya@gmail.com');

	// Save in the database
	$entityManager->persist($users);
	$entityManager->flush();

	echo "Created message with ID " . $users->getId() . "\n";
	
});


function checkMySQL(){
	$servername = "sharemyreads.com";
	$username = "kapil2708";
	$password = "Kapil@224414";

	// Create connection
	$conn = new mysqli($servername, $username, $password);

	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
	echo "Connected successfully";
}


function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}*/


$app->run();
?>