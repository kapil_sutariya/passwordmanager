<?php

require 'Slim/Slim.php';
use Slim\Slim;

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->get('/pages', function(){
	
		include_once 'config.php';
		
	    // connect to the database
        $dbh = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USERNAME, DB_PASSWORD);

        // a query get all the records from the users table
        $sql = 'SELECT * FROM page';

        // use prepared statements, even if not strictly required is good practice
        $stmt = $dbh->prepare( $sql );

        // execute the query
        $stmt->execute();

        // fetch the results into an array
        $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

        // convert to json
        $json = json_encode( $result );
		echo $json;
});

$app->get('/category', function(){
    $sql = "select DISTINCT category FROM page";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $category = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($category);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
});

$app->post('/pages', function(){
    $request = Slim::getInstance()->request();
    $page = json_decode($request->getBody());
    $sql = "INSERT INTO page (page, category, user, password, updated) VALUES (:page, :category, :user, :password, now())";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("page", $page->page);
        $stmt->bindParam("category", $page->category);
        $stmt->bindParam("user", $page->user);
        $stmt->bindParam("password", $page->password);

        $stmt->execute();
        $page->id = $db->lastInsertId();
        $db = null;
        echo json_encode($page);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
});

$app->put('/pages/:id', function($id){
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $page = json_decode($body);
    $sql = "UPDATE page SET page=:page, category=:category, user=:user, password=:password, updated=:updated WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("page", $page->page);
        $stmt->bindParam("category", $page->category);
        $stmt->bindParam("user", $page->user);
        $stmt->bindParam("password", $page->password);
        $stmt->bindParam("updated", $page->updated);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo json_encode($page);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
});

$app->delete('/pages/:id', function($id) {
    $sql = "DELETE FROM page WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
});

function getConnection() {

    include_once 'config.php';

    // connect to the database
    $dbh = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USERNAME, DB_PASSWORD);

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>